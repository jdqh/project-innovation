﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Topic
{
    string name;
    List<Question> questions;

    public Topic(string pText)
    {
        name = pText;
        questions = new List<Question>();
    }
    public void SetTopicName(string pText)
    {
        name = pText;
    }
    public void SetQuestion(string pText)
    {
        questions.Add(new Question(pText));
    }
}
