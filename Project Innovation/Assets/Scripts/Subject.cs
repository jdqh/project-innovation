﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subject : MonoBehaviour
{
    public string subjectName;
    List<Topic> topics;

    public Subject(string pText)
    {
        subjectName = pText;
        topics = new List<Topic>();
    }
    public void SetSubjectName(string pText)
    {
        name = pText;
    }
    public void SetTopic(string pText)
    {
        topics.Add(new Topic(pText));
    }
}
