﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Query
{
    public string question;
    public int amount;
    public string[] answers;
    public Dictionary<string, bool> answerDict;
    public List<string> storyBit;
    public Sprite storyImage;
    public Sprite characterImage;

    public Query(string pQuestion="", int pAmount = 4, Dictionary<string, bool> pAnswerDict= null)
    {
        question = pQuestion;
        amount = pAmount;
        answers = new string[pAmount];
        answerDict = pAnswerDict;
        if (answerDict == null) { answerDict = new Dictionary<string, bool>(); }
        storyBit = new List<string>();
        storyImage = null;
        characterImage = null;
    }
    public void SetAnswerAmount(int pAmount)
    {
        amount = pAmount;
    }

    public void SetQuestion(string pQuestion)
    {
        question = pQuestion;
    }
    public void SetAnswer(int qNumber, string pAnswer, bool pCorrectAns=false)
    {
        answers[qNumber] = pAnswer;
        if (answerDict.ContainsKey(answers[qNumber])) { answerDict[answers[qNumber]] = pCorrectAns; }
        else
        {
            answerDict.Add(answers[qNumber], pCorrectAns);
        }
    }

    public bool ReturnAnswer(int pInt)
    {
        string pString = answers[pInt];
        return answerDict[pString];
    }

    public void AddStoryBit(string pText)
    {
        storyBit.Add(pText);
    }

    public void SetStoryImage(Sprite pSprite)
    {
        storyImage = pSprite;
    }

    public void SetCharacter(Sprite pSprite)
    {
        characterImage = pSprite;
    }
}
