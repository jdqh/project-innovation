﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchRegisterCanvas : MonoBehaviour
{
    public GameObject scan;
    public GameObject participantList;
    public GameObject newParticipant;
    private GameObject currentCanvas;

    void Start()
    {
        currentCanvas = scan;
    }

    public void SwitchToScan()
    {
        SwitchTo(scan);
    }

    public void SwitchToParticipantList()
    {
        SwitchTo(participantList);
    }

    public void ToggleNewParticipant()
    {
        ToggleObject(newParticipant);
    }

    void SwitchTo(GameObject gameObject)
    {
        if (currentCanvas != null) { currentCanvas.SetActive(false); }
        currentCanvas = gameObject;
        currentCanvas.SetActive(true);
    }

    void ToggleObject(GameObject gameObject)
    {
        if (gameObject.activeSelf) { gameObject.SetActive(false); }
        else { gameObject.SetActive(true); }
    }
}
