﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Question
{
    string questionText;
    List<Answer> answers;

    public Question(string pText)
    {
        questionText = pText;
        answers = new List<Answer>();
    }

    public void SetQuestion(string pText)
    {
        questionText = pText;
    }
    public void SetAnswer(int pNumber, string pText, bool pBool)
    {
        //var apple = new Answer
        answers.Add(new Answer(pNumber, pText, pBool));
    }
}
