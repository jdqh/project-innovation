﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.IO.Ports;
public class ScannerReader : MonoBehaviour
{
    SerialPort sp = new SerialPort("COM6", 9600);
    static public string answer;

    void Start()
    {
        sp.Open();
        sp.ReadTimeout = 100;
    }

    void Update()
    {
        try
        {
            //print(sp.ReadLine());
            WriteString(sp.ReadLine());
            answer = sp.ReadLine();
            if(answer.Length == 1)
            {
                GetAnswer(answer);
            }
        }
        catch (System.Exception)
            {
            }

    }

    static void GetAnswer(string line)
    {
        Debug.Log(line);
        if (line.Equals("a"))
        {
            //Debug.Log("correct!");
        }
    }

    static void WriteString(string line)
    {
        string path = "Assets/Resources/Database.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(line);
        writer.Close();

        //Re-import the file to update the reference in the editor
        AssetDatabase.ImportAsset(path);
        TextAsset asset = Resources.Load("Database") as TextAsset;

        //Print the text from the file
        //Debug.Log(asset.text);
    }

    static void ReadString()
    {
        string path = "Assets/Resources/Database.txt";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        Debug.Log(reader.ReadToEnd());
        reader.Close();
    }
}
