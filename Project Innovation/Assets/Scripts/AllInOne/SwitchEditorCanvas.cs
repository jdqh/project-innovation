﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchEditorCanvas : MonoBehaviour
{
    public GameObject storyEditor;
    public GameObject questionEditor;
    public GameObject hintEditor;
    private GameObject currentCanvas;

    void Start()
    {
        TurnOff();
        SwitchToEditor();
    }
    public void SwitchToStory()
    {
        SwitchTo(storyEditor);
    }
    public void SwitchToEditor()
    {
        SwitchTo(questionEditor);
    }

    public void ToggleHint()
    {
        ToggleObject(hintEditor);
    }
    void SwitchTo(GameObject gameObject)
    {
        if (currentCanvas != null) { currentCanvas.SetActive(false); }
        currentCanvas = gameObject;
        currentCanvas.SetActive(true);
    }

    void ToggleObject(GameObject gameObject)
    {
        if (gameObject.activeSelf) { gameObject.SetActive(false); }
        else { gameObject.SetActive(true); }
    }

    void TurnOff()
    {
        storyEditor.SetActive(false);
        hintEditor.SetActive(false);
        questionEditor.SetActive(false);
    }
}
