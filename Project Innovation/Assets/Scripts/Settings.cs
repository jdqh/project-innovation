﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    static public bool quickAnswer=false;
    static public bool rowAnswer=false;
    static public bool hint=false;
    static public bool randomize=false;
    static public bool numbersVisuals=false;
    static public float answerTime=30;
    static public float hintPercentage =15;
    public bool RowAnswer { get => rowAnswer; set => rowAnswer = value; }
    public bool QuickAnswer { get => quickAnswer; set => quickAnswer = value; }
    public bool Hint { get => hint; set => hint = value; }
    public bool Randomize { get => randomize; set => randomize = value; }
    public bool NumbersVisuals { get => numbersVisuals; set => numbersVisuals = value; }
    public float AnswerTime { get => answerTime; set => answerTime = value; }
    public float HintPercentage { get => hintPercentage; set => hintPercentage = value; }

    public void ChangeTime(string pFloat)
    {
        answerTime = float.Parse(pFloat);
        print(answerTime);
    }
    public void ToggleQuick(bool pBool)
    {
        quickAnswer = pBool;
        print(quickAnswer);
    }
    public void ToggleRow(bool pBool)
    {
        rowAnswer = pBool;
        print(rowAnswer);
    }
    public void ToggleHint(bool pBool)
    {
        hint = pBool;
        print(hint);
    }
    public void ChangeHintPerc(string pFloat)
    {
        hintPercentage = float.Parse(pFloat);
        print(hintPercentage);
    }
    public void ToggleRandom(bool pBool)
    {
        randomize = pBool;
        print(randomize);
    }
    public void ToggleVisuals(bool pBool)
    {
        numbersVisuals = pBool;
        print(numbersVisuals);
    }
}
