﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct QueryList
{
    public List<Query> queryList;
}
