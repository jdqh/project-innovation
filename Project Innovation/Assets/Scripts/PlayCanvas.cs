﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayCanvas : MonoBehaviour
{
    public TextMeshProUGUI question;
    public TextMeshProUGUI answer1;
    public TextMeshProUGUI answer2;
    public TextMeshProUGUI answer3;
    public TextMeshProUGUI answer4;
    TextMeshProUGUI[] answers = new TextMeshProUGUI[4];
    int queryNumber=0;
    bool finishQuiz = false;
    List<Query> queryList;
    void Start()
    {
        queryList = QueryCanvas.queryList;
        answers[0] = answer1;
        answers[1] = answer2;
        answers[2] = answer3;
        answers[3] = answer4;
    }
    void Update()
    {
        SetQuestionAnswers();
    }

    void SetQuestionAnswers()
    {
        question.text = queryList[queryNumber].question;
        for (int i = 0; i < 4; ++i)
        {
            answers[i].text = queryList[queryNumber].answers[i];
        }
    }

    void NextQuestion()
    {
        if (queryNumber + 1 <= queryList.Count - 1)
        {
            ++queryNumber;
            print(queryNumber);
        }else if(queryNumber == queryList.Count)
        {
            finishQuiz = true;
        }
    }
    void PreviousQuestion()
    {
        if (queryNumber - 1 >= 0)
        {
            --queryNumber;
            print(queryNumber);
        }
    }
}