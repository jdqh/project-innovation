﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SubjectButtons : MonoBehaviour
{
    [SerializeField] GameObject button;
    public List<Subject> subjectList;
    List<GameObject> initButtons;

    public void InstantiateSubjects()
    {
        foreach (Subject s in subjectList)
        {
            GameObject temp = button;
            temp.transform.SetParent(this.transform);
            print(temp);
            temp.GetComponentInChildren<TextMeshProUGUI>().text = s.subjectName;
            initButtons.Add(Instantiate(temp, transform));
        }
    }

    public void SetButtons()
    {
        foreach(GameObject g in initButtons)
        {
            Button button = g.GetComponent<Button>();
            button.onClick.AddListener(Test);
        }
    }
    public void Test()
    {
        print("testing");
    }
}
