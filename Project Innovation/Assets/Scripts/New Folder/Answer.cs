﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Answer
{
    public int number;
    public string answerText;
    public bool correctAnswer;

    public Answer(int pNumber, string pText, bool pCorrectAnswer)
    {
        number = pNumber;
        answerText = pText;
        correctAnswer = pCorrectAnswer;
    }
    public void SetAnswer(string pText)
    {
        answerText = pText;
    }
    public void SetBool(bool pBool)
    {
        correctAnswer = pBool;
    }
}
