﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsScript : MonoBehaviour
{
    public GameObject winners;
    public GameObject achievements;
    public GameObject results;
    private GameObject currentObject;

    void Start()
    {
        currentObject = winners;
    }

    public void SwitchToWinners()
    {
        SwitchTo(winners);
    }

    public void SwitchToAchievements()
    {
        SwitchTo(achievements);
    }
    public void SwitchToResults()
    {
        SwitchTo(results);
    }

    void SwitchTo(GameObject gameObject)
    {
        if (currentObject != null) { currentObject.SetActive(false); }
        currentObject = gameObject;
        currentObject.SetActive(true);
    }
}
