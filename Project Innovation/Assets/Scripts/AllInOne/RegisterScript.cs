﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterScript : MonoBehaviour
{
    static List<Participant> participantList = new List<Participant>();
    Participant participant;

    void Start()
    {
        
    }
    public void SetFirstName(string fName)
    {
        participant.FirstName = fName;
    }

    public void SetLastName(string lName)
    {
        participant.LastName = lName;
    }

    public void SetBirthDate(int pInt)
    {
        participant.BirthDate = pInt;
    }

    public void SetClass(string pString)
    {
        participant.ClassName = pString;
    }

    public void AddParticipant()
    {
        participantList.Add(participant);
        ShowParticipant();
        participant = new Participant();
    }

    public void ShowParticipant()
    {
        print("Participant "+participant.FirstName+" "+participant.LastName+" added");
        
    }
}
