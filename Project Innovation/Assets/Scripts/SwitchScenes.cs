﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScenes : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchScene(string pText)
    {
        SceneManager.LoadScene(pText);
    }
    public void LoadEditorScene()
    {
        SceneManager.LoadScene("Editor");
    }
    public void LoadMenuScene()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void LoadPlayScene()
    {
        SceneManager.LoadScene("PlayScreen");
    }
    public void LoadRegisterScene()
    {
        SceneManager.LoadScene("Register");
    }
    public void LoadResultsScene()
    {
        SceneManager.LoadScene("Results");
    }
    public void LoadSettingsScene()
    {
        SceneManager.LoadScene("Settings");
    }

    //public void PreviousScene()
    //{
    //    SceneManager.
    //}
}
