﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayScript : MonoBehaviour
{
    //StoryPart
    public string storyText;
    public List<Query> queryList = QuestionEditor.queryList;
    static public int questionNumber;

    void Start()
    {
        SwitchToStory();
        if (queryList.Count == 0)
        {
            SwitchToError();
        }
        questionNumber = 0;
        if (queryList[questionNumber].storyBit.Count <= 0 || queryList[questionNumber].storyBit == null){
            SwitchToPlay();
        }
        print("jsajdjsaf");
    }

    void Update()
    {
        if(questionNumber > queryList.Count - 1)
        {
            SwitchToStats();
        }
    }

    public void NextStoryPage()
    {
        int i = 0;
        if (i<= queryList[questionNumber].storyBit.Count-1 && queryList[questionNumber].storyBit.Count>0)
        {
            storyText = queryList[questionNumber].storyBit[i];
            ++i;
        }
        else
        {
            SwitchToPlay();
        }
    }


    //QuizPart
    public void NextQuestion()
    {
        ++questionNumber;
    }

    //StatisticsPart

    
    
    //Switch
    public GameObject story;
    public GameObject play;
    public GameObject statistics;
    public GameObject error;
    public GameObject currentObject;
    public GameObject confetti;

    public void SwitchToStory()
    {
        SwitchTo(story);
    }
    public void SwitchToPlay()
    {
        SwitchTo(play);
    }
    public void SwitchToStats()
    {
        SwitchTo(statistics);
        confetti.SetActive(true);
    }

    void SwitchTo(GameObject gameObject)
    {
        if (currentObject != null) { currentObject.SetActive(false); }
        currentObject = gameObject;
        currentObject.SetActive(true);
    }
    void ToggleObject(GameObject gameObject)
    {
        if (gameObject.activeSelf) { gameObject.SetActive(false); }
        else { gameObject.SetActive(true); }
    }

    public void SwitchToError()
    {
        SwitchTo(error);
    }
} 
