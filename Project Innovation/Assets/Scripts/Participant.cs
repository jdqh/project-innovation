﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Participant
{
    private string scannedID;
    private string firstName;
    private string lastName;
    private int birthDate;
    private string className;
    private int score;
    private float time;
    private float averageTime;
    private int unpopularVote;
    private int streak;
    private int longestStreak;

    public string FirstName { get => firstName; set => firstName = value; }
    public string LastName { get => lastName; set => lastName = value; }
    public int BirthDate { get => birthDate; set => birthDate = value; }
    public string ClassName { get => className; set => className = value; }
    public int Score { get => score; set => score = value; }
    public float Time { get => time; set => time = value; }
    public float AverageTime { get => averageTime; set => averageTime = value; }
    public int UnpopularVote { get => unpopularVote; set => unpopularVote = value; }
    public int Streak { get => streak; set => streak = value; }
    public int LongestStreak { get => longestStreak; set => longestStreak = value; }
    public string ScannedID { get => scannedID; set => scannedID = value; }
}
