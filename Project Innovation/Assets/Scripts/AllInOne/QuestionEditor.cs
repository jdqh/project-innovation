﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestionEditor : MonoBehaviour
{
    public GameObject buttonRect;
    public GameObject buttonPrefab;
    public GameObject questionText;
    public GameObject answerText1;
    public GameObject answerText2;
    public GameObject answerText3;
    public GameObject answerText4;
    public Query query = new Query("", 4);
    static public List<Query> queryList = new List<Query>();
    private Dictionary<string, bool> answerDict;
    private string answer1;
    private bool bool1=false;
    private string answer2;
    private bool bool2=false;
    private string answer3;
    private bool bool3=false;
    private string answer4;
    private bool bool4=false;
    static public int questionNumber = 0;

    public GameObject button;
    List<GameObject> buttonList = new List<GameObject>();
    Dictionary<GameObject, int> buttonNumber = new Dictionary<GameObject, int>();
    List<int> buttonIntList = new List<int>();
    int i = 0;
    void CreateButton()
    {
        button = buttonPrefab;
        button.GetComponent<Button>().onClick.AddListener(ButtonMethod);
        buttonNumber.Add(button, i);
        ++i;
        buttonList.Add(Instantiate(buttonPrefab));
    }

    void ButtonMethod()
    {
        //questionNumber = buttonNumber[];
    }

    public void NewQuery()
    {
        query = new Query();
    }
    public void PreviousQuestion()
    {
        if (questionNumber - 1 >= 0)
        {
            --questionNumber;
        }
        if (questionNumber <= 0)
        {
            questionNumber = 0;
        }
        print(questionNumber);
    }
    public void NextQuestion()
    {
        if (questionNumber + 1 <= queryList.Count)
        {
            ++questionNumber;
        }
        if (questionNumber >= queryList.Count)
        {
            AddQuery();
            //NewQuery();
        }
        print(questionNumber);
    }

    public void ShowQuestion()
    {
        questionText.GetComponentInChildren<TMP_InputField>().text = queryList[questionNumber].question;
    }
    public void ShowAnswers()
    {
        for (int i = 0; i < 4; ++i)
        {
            print(queryList[questionNumber].answers[i]);
        }
        answerText1.GetComponentInChildren<TMP_InputField>().text = queryList[questionNumber].answers[0];
        answerText2.GetComponentInChildren<TMP_InputField>().text = queryList[questionNumber].answers[1];
        answerText3.GetComponentInChildren<TMP_InputField>().text = queryList[questionNumber].answers[2];
        answerText4.GetComponentInChildren<TMP_InputField>().text = queryList[questionNumber].answers[3];
    }
    void Update()
    {

    }

    public void ShowQuery()
    {
        print(query.question);
        for (int i = 0; i < 4; ++i)
        {
            print(query.answers[i]);
            print(query.ReturnAnswer(i));
        }
    }
    public void SetQuestion(string pString)
    {
        query.SetQuestion(pString);
    }
    public void SetAnswer1(string pString)
    {
        query.SetAnswer(0, pString);
    }
    public void SetAnswer2(string pString)
    {
        query.SetAnswer(1, pString);
    }
    public void SetAnswer3(string pString)
    {
        query.SetAnswer(2, pString);
    }
    public void SetAnswer4(string pString)
    {
        query.SetAnswer(3, pString);
    }

    public void SetCorrect1(bool pBool)
    {
        query.SetAnswer(0, query.answers[0], pBool);
    }
    public void SetCorrect2(bool pBool)
    {
        query.SetAnswer(1, query.answers[1], pBool);
    }
    public void SetCorrect3(bool pBool)
    {
        query.SetAnswer(2, query.answers[2], pBool);
    }
    public void SetCorrect4(bool pBool)
    {
        query.SetAnswer(3, query.answers[3], pBool);
    }
    public void AddQuery()
    {
        queryList.Add(query);
        print(queryList.Count);
    }
}
