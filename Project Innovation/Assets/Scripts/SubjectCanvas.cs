﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SubjectCanvas : MonoBehaviour
{
    public List<Subject> subjects;
    public List<GameObject> initButtons;

    void Start()
    {
        if (subjects == null) { subjects = new List<Subject>(); }
        if (initButtons == null) { initButtons = new List<GameObject>(); }
    }
    public void AddSubject(string pText)
    {
        subjects.Add(new Subject(pText));
    }
    public void EditSubjectName(Subject sub, string pName)
    {
        sub.name = pName;
    }
    public void UpdateList()
    {
        foreach (GameObject g in initButtons)
        {

        }
    }

    public void SetButtonList()
    {
        GetComponentInChildren<SubjectButtons>().subjectList = subjects;
    }
}
