﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchCanvas : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject PlayMenu;
    public GameObject SettingsMenu;
    public GameObject ResultsMenu;
    public GameObject EditorMenu;
    public GameObject PremadeMenu;
    public GameObject RegisterMenu;
    private GameObject currentMenu;

    void Start()
    {
        currentMenu = new GameObject();
        SwitchToMain();
    }
    public void SwitchToMain()
    {
        SwitchTo(MainMenu);
    }
    public void SwitchToSettings()
    {
        SwitchTo(SettingsMenu);
    }
    public void SwitchToResults()
    {
        SwitchTo(ResultsMenu);
    }
    public void SwitchToEditor()
    {
        SwitchTo(EditorMenu);
    }
    public void SwitchToPremade()
    {
        SwitchTo(PremadeMenu);
    }
    public void SwitchToRegister()
    {
        SwitchTo(RegisterMenu);
    }
    public void SwitchToPlay()
    {
        SwitchTo(PlayMenu);
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void SwitchTo(GameObject gameObject)
    {
        if(currentMenu != null) { currentMenu.SetActive(false); }
        currentMenu = gameObject;
        currentMenu.SetActive(true);
    }
}