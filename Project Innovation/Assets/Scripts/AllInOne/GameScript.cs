﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameScript : MonoBehaviour
{
    public GameObject nextButton;
    public TextMeshProUGUI question;
    public TextMeshProUGUI answer1;
    public TextMeshProUGUI answer2;
    public TextMeshProUGUI answer3;
    public TextMeshProUGUI answer4;
    TextMeshProUGUI[] answers = new TextMeshProUGUI[4];
    int queryNumber = 0;
    bool finishQuiz = false;
    public string answer;
    List<Query> queryList;
    float timer;

    void Start()
    {
        nextButton.SetActive(true);
        answers[0] = answer1;
        answers[1] = answer2;
        answers[2] = answer3;
        answers[3] = answer4;
        queryList = QuestionEditor.queryList;
        queryNumber = QuestionEditor.questionNumber;
        timer = 0;
        SetQuestionAnswers();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        queryList = QuestionEditor.queryList;
        queryNumber = QuestionEditor.questionNumber;

        //print(timer);

        CheckCorrectAnswer();

        if(timer >= Settings.answerTime)
        {
            nextButton.SetActive(true);
            //timer = 0;
        }
    }

    public void SetQuestionAnswers()
    {
        print(queryList[queryNumber].question);
        question.text = queryList[queryNumber].question;
        for (int i = 0; i < 4; ++i)
        {
            answers[i].text = queryList[queryNumber].answers[i];
        }
    }

    public void CheckCorrectAnswer()
    {
        for (int i = 0; i < 4; ++i)
        {
            print("question number"+queryNumber);
            print("list size"+queryList.Count);

            if (queryList[queryNumber].answerDict[queryList[queryNumber].answers[i]])
            {
                switch (i)
                {
                    case 0:
                        answer = "a";
                        break;
                    case 1:
                        answer = "b";
                        break;
                    case 2:
                        answer = "c";
                        break;
                    case 3:
                        answer = "d";
                        break;
                }
            }
        }
        print(answer);
        if(answer == ScannerReader.answer)
        {
            correctChoice = true;
            print("correct");
        }
        else
        {
            print(ScannerReader.answer);
        }
    }
    public bool correctChoice=false;
}
