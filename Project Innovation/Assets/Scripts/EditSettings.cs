﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditSettings : MonoBehaviour
{
    Settings settings;

    void ChangeQuick(bool pBool)
    {
        settings.QuickAnswer = pBool;
    }
    void RowQuick(bool pBool)
    {
        settings.RowAnswer = pBool;
    }
    void ChangeHint(bool pBool)
    {
        settings.Hint = pBool;
    }
    void ChangeRandom(bool pBool)
    {
        settings.Randomize = pBool;
    }
    void ChangeVisuals(bool pBool)
    {
        settings.NumbersVisuals = pBool;
    }

    void ChangeTime(float pFloat)
    {
        settings.AnswerTime = pFloat;
    }
    void ChangePerc(float pFloat)
    {
        settings.HintPercentage = pFloat;
    }
}
